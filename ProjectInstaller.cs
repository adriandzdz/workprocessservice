﻿using System.ComponentModel;
using System.Configuration;

namespace WorkProcessService
{
	[RunInstaller(true)]
	public partial class ProjectInstaller : System.Configuration.Install.Installer
	{
		public ProjectInstaller()
		{
			InitializeComponent();

			serviceInstaller.Description = ConfigurationManager.AppSettings["ServiceInstallerDescription"];
			serviceInstaller.DisplayName = ConfigurationManager.AppSettings["ServiceInstallerDisplayName"];

		}
	}
}
