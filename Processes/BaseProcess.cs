﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace WorkProcessService.Processes
{
	public abstract class BaseProcess
	{
		private readonly object _syncRoot = new object();
		private CancellationTokenSource _tokenSource;
		protected int _minutesOfExecution;
		private DateTime _lastExecution;
		protected EventLog EventLog;

		protected BaseProcess()
		{
			EventLog = EventLog ?? new EventLog(ConfigurationManager.AppSettings["EventLogApplication"], ".", ConfigurationManager.AppSettings["EventLogSources"]);
		}

		protected abstract void Execute();

		public virtual void ExecuteProcess()
		{
			lock (_syncRoot)
			{
				_tokenSource?.Cancel();

				_lastExecution = DateTime.Now;

				_tokenSource = new CancellationTokenSource();
				Task.Factory.StartNew(() => { Process(_tokenSource.Token); }, _tokenSource.Token, TaskCreationOptions.LongRunning, TaskScheduler.Default);
			}
		}

		private void Process(CancellationToken token)
		{
			while (!token.IsCancellationRequested)
			{
				if (_lastExecution.AddMinutes(_minutesOfExecution) > DateTime.Now)
				{
					Thread.Sleep(1000);
					continue;
				}

				_lastExecution = DateTime.Now;

				Execute();
			}
		}

	}
}
