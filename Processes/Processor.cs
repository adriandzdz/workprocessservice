﻿using System.Collections.Generic;

namespace WorkProcessService.Processes
{
	public class Processor
	{
		private static Processor _instance;

		public Processor()
		{

		}

		public static Processor Instance
		{
			get { return _instance ?? (_instance = new Processor()); }
		}

		public void Process(List<BaseProcess> processors)
		{
			foreach (var processor in processors)
			{
				processor.ExecuteProcess();
			}
		}
	}
}
