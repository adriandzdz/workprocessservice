﻿using System.Collections.Generic;
using System.ServiceProcess;
using WorkProcessService.Processes;

namespace WorkProcessService
{
	internal class Program
	{
		static void Main()
		{
			ServiceBase[] ServicesToRun;
			var processors = new List<BaseProcess>();
			ServicesToRun = new ServiceBase[]
			{
				new ServiceProcessor(processors)
			};
			ServiceBase.Run(ServicesToRun);
		}
	}
}
