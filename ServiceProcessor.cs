﻿using System.ServiceProcess;
using System.Diagnostics;
using WorkProcessService.Properties;
using System;
using WorkProcessService.Processes;
using System.Collections.Generic;
using System.Configuration;

namespace WorkProcessService
{
	public partial class ServiceProcessor : ServiceBase
	{
		private readonly List<BaseProcess> _processors;
		public ServiceProcessor(List<BaseProcess> processors)
		{
			_processors = processors;

			InitializeComponent();

			if (!EventLog.SourceExists(ConfigurationManager.AppSettings["EventLogSources"]))
			{
				EventLog.CreateEventSource(ConfigurationManager.AppSettings["EventLogSources"], ConfigurationManager.AppSettings["EventLogApplication"]);
			}
		}

		protected override void OnStart(string[] args)
		{
			eventLog.WriteEntry(Resource.StartProcess);
			try
			{
				Processor.Instance.Process(_processors);
			}
			catch (Exception ex)
			{
				eventLog.WriteEntry(string.Format(Resource.StartError, ex));
				throw;
			}
		}

		protected override void OnStop()
		{
			eventLog.WriteEntry(Resource.StopProcess);
		}
	}
}
